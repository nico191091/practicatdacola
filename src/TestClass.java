import ar.com.uade.algoritmos.EjercicioCola;
import ar.com.uade.impl.TDAColaImpl;
import ar.com.uade.tda.TDACola;


public class TestClass {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		
		TDACola cola1 = new TDAColaImpl();
		TDACola cola2 = new TDAColaImpl();
		EjercicioCola ej = new EjercicioCola();
		cola1.inicializar();
		cola1.acolar(1);
		cola1.acolar(7);
		cola1.acolar(3);
		cola1.acolar(2);
		cola1.acolar(5);
		cola1.imprimir();
		
		cola2.inicializar();
		cola2.acolar(5);
		cola2.acolar(2);
		cola2.acolar(3);
		cola2.acolar(7);
		cola2.acolar(1);
		cola2.imprimir();
		
		boolean esInversa = ej.esInversa(cola1,cola2);
	}

}
