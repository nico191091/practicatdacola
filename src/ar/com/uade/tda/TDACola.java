package ar.com.uade.tda;

public interface TDACola {
	
	public void inicializar();
	
	/** 
	 * Debe estar inicializada
	 *  */
	public void acolar(int valor);
	
	/** 
	 * Debe estar inicializada y no vacia
	 *  */
	public void desacolar();
	
	/** 
	 * Debe estar inicializada
	 *  */
	public boolean colaVacia();
	
	/** 
	 * Debe estar inicializada y no vacia
	 *  */
	public int primero();
	
	public void imprimir();
}
